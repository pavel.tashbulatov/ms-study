package ru.study.socialnetwork.controllers;

import org.springframework.web.bind.annotation.*;
import ru.study.socialnetwork.entities.Comment;
import ru.study.socialnetwork.services.CommentService;

import java.util.List;

@RestController
@RequestMapping(value = "/comments")
public class CommentsController {
    private final CommentService commentService;

    public CommentsController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    String createComment(@RequestBody Comment comment) {
        return commentService.createComment(comment);
    }

    @GetMapping(path = "/{id}")
    Comment getComment(@PathVariable Long id) {
        return commentService.getComment(id);
    }

    @PutMapping(path = "/{id}")
    String updateComment(@RequestBody Comment comment, @PathVariable Long id) {
        return commentService.updateComment(comment, id);
    }

    @DeleteMapping(path = "/{id}")
    String deleteComment(@PathVariable Long id) {
        return commentService.deleteComment(id);
    }

    @GetMapping
    List<Comment> getCommentsByPostAndParent(@RequestParam Long postId, @RequestParam(required = false) Long parentId) {
        return commentService.getCommentsByPostAndParent(postId, parentId);
    }
}
