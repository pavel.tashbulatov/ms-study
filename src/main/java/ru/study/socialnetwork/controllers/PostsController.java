package ru.study.socialnetwork.controllers;

import org.springframework.web.bind.annotation.*;
import ru.study.socialnetwork.entities.Post;
import ru.study.socialnetwork.services.PostService;

import java.util.List;

@RestController
@RequestMapping(value = "/posts")
public class PostsController {
    private final PostService postService;

    public PostsController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    String createPost(@RequestBody Post post) {
        return postService.createPost(post);
    }

    @GetMapping(path = "/{id}")
    Post getPost(@PathVariable Long id) {
        return postService.getPost(id);
    }

    @PutMapping(path = "/{id}")
    String updatePost(@RequestBody Post post, @PathVariable Long id) {
        return postService.updatePost(post, id);
    }

    @DeleteMapping(path = "/{id}")
    String deletePost(@PathVariable Long id) {
        return postService.deletePost(id);
    }

    @GetMapping(path = "/user/{id}")
    List<Post> getPostsByUserId(@PathVariable Long id) {
        return postService.getPostsByUserId(id);
    }
}
