package ru.study.socialnetwork.controllers;

import org.springframework.web.bind.annotation.*;
import ru.study.socialnetwork.entities.Subscription;
import ru.study.socialnetwork.services.SubscriptionService;

import java.util.List;

@RestController
@RequestMapping(path = "/subscriptions")
public class SubscriptionsController {
    
    private final SubscriptionService subscriptionService;

    public SubscriptionsController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping
    String createSubscription(@RequestBody Subscription subscription) {
        return subscriptionService.createSubscription(subscription);
    }

    @GetMapping(path = "/{id}")
    Subscription getSubscription(@PathVariable Long id) {
        return subscriptionService.getSubscription(id);
    }

    @PutMapping(path = "/{id}")
    String updateSubscription(@RequestBody Subscription subscription, @PathVariable Long id) {
        return subscriptionService.updateSubscription(subscription, id);
    }

    @DeleteMapping(path = "/{id}")
    String deleteSubscription(@PathVariable Long id) {
        return subscriptionService.deleteSubscription(id);
    }

    @GetMapping(path = "/user/{userId}")
    List<Long> getUserSubscriptions(@PathVariable Long userId) {
        return subscriptionService.getSubscriptionsByUserId(userId);
    }

    @GetMapping(path = "/subscribers/{userId}")
    List<Long> getUserSubscribers(@PathVariable Long userId) {
        return subscriptionService.getSubscriptionsBySubscriptionId(userId);
    }

    @DeleteMapping(path = "/unsubscribe/{userId}")
    String unsubscribeFromUser(@PathVariable Long userId, @RequestParam Long subscriptionId) {
        System.out.println(">>>" + userId + "   " + subscriptionId);
        return subscriptionService.unsubscribe(userId, subscriptionId);
    }

}
