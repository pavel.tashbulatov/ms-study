package ru.study.socialnetwork.entities;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "comments")
@SQLDelete(sql = "UPDATE comments SET deleted = true WHERE id=?")
@FilterDef(name = "deletedCommentFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedCommentFilter", condition = "deleted = :isDeleted")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "post_id")
    private Long postId;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "likes")
    private Integer likes;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "content")
    private String content;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public Long getPostId() {
        return postId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getLikes() {
        return likes;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getContent() {
        return content;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public Comment() {}

    public Comment(Long postId, Long parentId, String content) {
        this.postId = postId;
        this.parentId = parentId;
        this.content = content;
    }
}
