package ru.study.socialnetwork.entities;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "subscriptions")
@SQLDelete(sql = "UPDATE subscriptions SET deleted = true WHERE id=?")
@FilterDef(name = "deletedSubscriptionFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedSubscriptionFilter", condition = "deleted = :isDeleted")
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "subscription_id")
    private Long subscriptionId;

    @Column(name = "date")
    private Date date;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDate() {
        return date;
    }

    public Subscription() {}

    public boolean isDeleted() {
        return deleted;
    }

    public Subscription(Long userId, Long subscriptionId) {
        this.userId = userId;
        this.subscriptionId = subscriptionId;
        this.date = new Date();
    }
}
