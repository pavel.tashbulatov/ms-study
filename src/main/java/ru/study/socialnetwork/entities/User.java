package ru.study.socialnetwork.entities;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "users")
@SQLDelete(sql = "UPDATE users SET deleted = true WHERE id=?")
@FilterDef(name = "deletedUserFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedUserFilter", condition = "deleted = :isDeleted")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "user_name")
    private String name;

    @Column(name = "last_seen_date")
    private Date lastSeenDate;

    @Column(name = "info")
    private String info;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public Date getLastSeenDate() {
        return lastSeenDate;
    }

    public String getInfo() {
        return info;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public User() {}

    public User(String login, String name, String info, String avatarUrl) {
        this.login = login;
        this.name = name;
        this.lastSeenDate = new Date();
        this.info = info;
        this.avatarUrl = avatarUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", lastSeenDate='" + lastSeenDate + '\'' +
                ", info='" + info + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                '}';
    }
}
