package ru.study.socialnetwork.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.study.socialnetwork.entities.Comment;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findCommentByPostIdAndParentId(Long postId, Long parentId);
}
