package ru.study.socialnetwork.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.study.socialnetwork.entities.Post;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Long> {
    List<Post> findPostByUserId(Long userId);
}
