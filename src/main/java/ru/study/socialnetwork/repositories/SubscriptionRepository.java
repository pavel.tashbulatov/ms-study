package ru.study.socialnetwork.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.study.socialnetwork.entities.Subscription;

import java.util.List;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
    List<Subscription> findAllByUserId(Long userId);

    List<Subscription> findAllBySubscriptionId(Long subscriptionId);

    boolean existsByUserIdAndSubscriptionId(Long userId, Long subscriptionId);

    Subscription findByUserIdAndSubscriptionIdAndDeletedIsFalse(Long userId, Long subscriptionId);
}
