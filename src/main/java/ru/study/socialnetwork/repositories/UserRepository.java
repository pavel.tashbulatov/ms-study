package ru.study.socialnetwork.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.study.socialnetwork.entities.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByLogin(String lastName);

    List<User> findAll();
}
