package ru.study.socialnetwork.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.study.socialnetwork.entities.Comment;
import ru.study.socialnetwork.repositories.CommentRepository;

import java.util.List;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public String createComment(Comment comment) {
        Comment newComment = new Comment(comment.getPostId(), comment.getParentId(), comment.getContent());
        Comment savedComment = commentRepository.save(newComment);

        return String.format("New comment was added to db with id = %s", savedComment.getId());
    }
    public Comment getComment(Long commentId) {
        return commentRepository.findById(commentId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public String updateComment(Comment comment, long id) {
        if (!commentRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Comment savedComment = commentRepository.save(comment);

        return String.format("Comment with id %s was successfully updated", savedComment.getId());
    }

    public String deleteComment(long id) {
        if (!commentRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        commentRepository.deleteById(id);

        return String.format("Comment with %s was successfully deleted", id);
    }

    public List<Comment> getCommentsByPostAndParent(Long postId, Long parentId) {
        return commentRepository.findCommentByPostIdAndParentId(postId, parentId);
    }
}
