package ru.study.socialnetwork.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.study.socialnetwork.entities.Post;
import ru.study.socialnetwork.repositories.PostRepository;

import java.util.List;

@Service
public class PostService {
    private final PostRepository postRepository;

    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public String createPost(Post post) {
        Post newPost = new Post(post.getUserId(), post.getTitle(), post.getContent());
        Post savedPost = postRepository.save(newPost);

        return String.format("Post '%s' was added to db with id = %s", savedPost.getTitle(), savedPost.getId());
    }

    public Post getPost(Long postId) {
        return postRepository.findById(postId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public String updatePost(Post post, long id) {
        if (!postRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Post savedPost = postRepository.save(post);

        return String.format("Post '%s' was successfully updated", savedPost.getTitle());
    }

    public String deletePost(long id) {
        if (!postRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        postRepository.deleteById(id);

        return String.format("Post with %s was successfully deleted", id);
    }

    public List<Post> getPostsByUserId(Long userId) {
        return postRepository.findPostByUserId(userId);
    }
}
