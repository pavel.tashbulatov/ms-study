package ru.study.socialnetwork.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.study.socialnetwork.entities.Subscription;
import ru.study.socialnetwork.repositories.SubscriptionRepository;

import java.util.List;

@Service
public class SubscriptionService {
    private final SubscriptionRepository subscriptionRepository;

    public SubscriptionService(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }

    public String createSubscription(Subscription subscription) {
        Subscription newSubscription = new Subscription(subscription.getUserId(), subscription.getSubscriptionId());
        Subscription savedSubscription = subscriptionRepository.save(newSubscription);

        return String.format("New subscription was added to db with id = %s", savedSubscription.getSubscriptionId());
    }

    public Subscription getSubscription(Long id) {
        return subscriptionRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public String updateSubscription(Subscription subscription, long id) {
        if (!subscriptionRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Subscription savedSubscription = subscriptionRepository.save(subscription);

        return String.format("Subscription with id %s was successfully updated", savedSubscription.getUserId());
    }

    public String deleteSubscription(long id) {
        if (!subscriptionRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        subscriptionRepository.deleteById(id);

        return String.format("Subscription with %s was successfully deleted", id);
    }

    public List<Long> getSubscriptionsByUserId(Long userId) {
        return subscriptionRepository.findAllByUserId(userId).stream().map(Subscription::getUserId).toList();
    }

    public List<Long> getSubscriptionsBySubscriptionId(Long subscriptionId) {
        return subscriptionRepository.findAllBySubscriptionId(subscriptionId).stream().map(Subscription::getSubscriptionId).toList();
    }

    public String unsubscribe(Long userId, Long subscriptionId) {
        if (!subscriptionRepository.existsByUserIdAndSubscriptionId(userId, subscriptionId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Subscription foundSubscription = subscriptionRepository.findByUserIdAndSubscriptionIdAndDeletedIsFalse(userId, subscriptionId);
        subscriptionRepository.deleteById(foundSubscription.getId());

        return String.format("User with id %s was successfully unsubscribed from user with id %s", userId, subscriptionId);
    }
}
