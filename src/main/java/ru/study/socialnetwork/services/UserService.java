package ru.study.socialnetwork.services;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.study.socialnetwork.entities.User;
import ru.study.socialnetwork.repositories.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final EntityManager entityManager;

    public UserService(UserRepository userRepository, EntityManager entityManager) {
        this.userRepository = userRepository;
        this.entityManager = entityManager;
    }

    public String createUser(User user) {
        User newUser = new User(user.getLogin(), user.getName(), user.getInfo(), user.getAvatarUrl());
        User savedUser = userRepository.save(newUser);

        return String.format("%s was added to db with id = %s", savedUser.getName(), savedUser.getId());
    }

    public User getUser(long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public String updateUser(User user, long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        User savedUser = userRepository.save(user);

        return String.format("User %s was successfully updated", savedUser.getLogin());
    }

    public String deleteUser(long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        userRepository.deleteById(id);

        return String.format("User with %s was successfully deleted", id);
    }

    public List<User> getUsers(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedUserFilter");
        filter.setParameter("isDeleted", isDeleted);
        List<User> users =  userRepository.findAll();
        session.disableFilter("deletedUserFilter");

        return users;
    }
}
